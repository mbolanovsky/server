package main

import "time"

type Version struct {
	AppVersion string `json:"version"`
	Commit     string `json:"commit"`
	BuildTime  string `json:"buildTime"`
}

type ErrorResponse struct {
	Error string `json:"error"`
}

type Book struct {
	Authors []Author `json:"authors"`
}

type Author struct {
	Key string `json:"key"`
}

type AuthorResponse struct {
	FullName     string `json:"fuller_name"`
	PersonalName string `json:"personal_name"`
	Name         string `json:"name"`
}

type EndpointAuthorResponse struct {
	AuthorName string `json:"author"`
	Key        string `json:"authorKey"`
}

type Works struct {
	Entries []Entry `json:"entries"`
}

type Entry struct {
	Key      string      `json:"key"`
	Name     string      `json:"title"`
	Revision uint64      `json:"revision"`
	Created  CreatedTime `json:"created"`
}

func (e *Entry) ConvertToWork() (*Work, error) {
	created, err := time.Parse("2006-01-02T15:04:05.999999999", e.Created.Value)
	if err != nil {
		return nil, err
	}
	return &Work{
		Name:        e.Name,
		Key:         e.Key,
		Revision:    e.Revision,
		PublishDate: created.Format(time.RFC3339),
	}, nil
}

type CreatedTime struct {
	Value string `json:"value"`
}

type Work struct {
	Name        string `json:"name"`
	PublishDate string `json:"publishDate"`
	Revision    uint64 `json:"revisions"`
	Key         string `json:"key"`
}
