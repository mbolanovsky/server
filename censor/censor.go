package censor

import (
	"fmt"
	"sync"
)

type exist struct{}

var censor *Censor
var censorMX sync.Mutex

type Censor struct {
	authors map[string]exist
	mutex   sync.RWMutex
}

func NewCensor() *Censor {
	censorMX.Lock()
	defer censorMX.Unlock()

	if censor == nil {
		censor = &Censor{
			authors: map[string]exist{},
		}
	}

	return censor
}

func (c *Censor) Add(authorKey string) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	c.authors[authorKey] = exist{}
}

func (c *Censor) IsBlock(authorKey string) bool {
	c.mutex.RLock()
	defer c.mutex.RUnlock()

	fmt.Println(c.authors)

	_, ok := c.authors[authorKey]
	return ok
}
