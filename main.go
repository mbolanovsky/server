package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"server/censor"
)

var (
	version, commit, buildTime string
	// don't edit
	appVersionResponse []byte
)

func sendError(w http.ResponseWriter, httpCode int, err error) {
	buf, locErr := json.Marshal(ErrorResponse{Error: err.Error()})
	if locErr != nil {
		log.Println("origin error", err)
		log.Println("json marshall error", locErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(httpCode)
	w.Write(buf)
}

func enpGetAPPVersion(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	}

	var err error
	_, err = w.Write(appVersionResponse)
	if err != nil {
		log.Println(err)
	}
}

func enpGetAuthors(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	}

	bookISBN := req.URL.Query().Get("book")
	if bookISBN == "" {
		sendError(w, http.StatusBadRequest, errors.New("missing URL param book which has to contain isbn of book"))
		return
	}

	authors, err := getAuthorsKey(bookISBN)
	if err != nil {
		sendError(w, http.StatusInternalServerError, errors.New("cannot find author of the book"))
		return
	}

	result := make([]EndpointAuthorResponse, len(authors))
	for index := range authors {
		result[index].AuthorName, err = getAuthorsName(authors[index].Key)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err)
			return
		}
		result[index].Key = strings.TrimPrefix(authors[index].Key, "/authors/")
	}

	buf, err := json.Marshal(result)
	if err != nil {
		log.Println(err)
		sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
	}

	_, err = w.Write(buf)
	if err != nil {
		log.Println(err)
	}
}

func getAuthorsKey(isbn string) ([]Author, error) {
	resp, err := http.Get("https://openlibrary.org/isbn/" + isbn + ".json")
	if err != nil {
		return nil, err
	}

	defer close(resp.Body)

	decoder := json.NewDecoder(resp.Body)

	book := Book{}
	err = decoder.Decode(&book)
	if err != nil {
		return nil, err
	}

	return book.Authors, nil
}

func getAuthorsName(key string) (string, error) {
	resp, err := http.Get("https://openlibrary.org" + key + ".json")
	if err != nil {
		return "", nil
	}

	defer close(resp.Body)

	decoder := json.NewDecoder(resp.Body)

	author := AuthorResponse{}
	err = decoder.Decode(&author)
	if err != nil {
		return "", nil
	}

	if author.FullName != "" {
		return author.FullName, nil
	} else if author.PersonalName != "" {
		return author.PersonalName, nil
	}

	return author.Name, nil
}

func close(reader io.ReadCloser) {
	err := reader.Close()
	if err != nil {
		log.Println(err)
	}
}

func enpGetWorks(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	}

	authorId := req.URL.Query().Get("authorId")
	if authorId == "" {
		sendError(w, http.StatusBadRequest, errors.New("missing URL param authorId which has to contain OL id of authors"))
		return
	}

	censorRegister := censor.NewCensor()
	if censorRegister.IsBlock(authorId) {
		sendError(w, http.StatusForbidden, fmt.Errorf("author %s is blocked", authorId))
		return
	}

	resp, err := http.Get("https://openlibrary.org/authors/" + authorId + "/works.json")
	if err != nil {
		sendError(w, http.StatusInternalServerError, err)
		return
	}

	defer close(resp.Body)

	decoder := json.NewDecoder(resp.Body)
	works := Works{}
	err = decoder.Decode(&works)
	if err != nil {
		sendError(w, http.StatusBadRequest, err)
		return
	}

	result := make([]*Work, len(works.Entries))
	for index := range works.Entries {
		result[index], err = works.Entries[index].ConvertToWork()
		if err != nil {
			sendError(w, http.StatusInternalServerError, err)
			return
		}
	}

	buf, err := json.Marshal(result)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err)
		return
	}

	_, err = w.Write(buf)
	if err != nil {
		log.Println(err)
	}
}

func enpAddCensors(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	}

	decoder := json.NewDecoder(req.Body)
	authors := []string{}
	err := decoder.Decode(&authors)
	if err != nil {
		sendError(w, http.StatusBadRequest, err)
		return
	}

	censorRegister := censor.NewCensor()
	for _, author := range authors {
		censorRegister.Add(author)
	}
}

func main() {
	var err error
	appVersionResponse, err = json.Marshal(&Version{AppVersion: version, BuildTime: buildTime, Commit: commit})
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/api/v1/version", enpGetAPPVersion)
	http.HandleFunc("/api/v1/authors", enpGetAuthors)
	http.HandleFunc("/api/v1/books", enpGetWorks)
	http.HandleFunc("/api/v1/censors", enpAddCensors)

	err = http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}

// curl localhost:8080/api/v1/authors?book=9780140328721
